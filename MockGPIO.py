OUT = 'out'
IN = 'in'
HIGH = 1
LOW = 0

def setup(pinname, direction):
   print('setup:' +pinname + ' ' + direction)

def direction(pinname, direction):
   print('direction:' +pinname + ' ' + direction)

def output(pinname, value):
   print('output:' +pinname + ' ' + str(value))

def cleanup(pinname):
   print('cleanup:' +pinname)