function submitFormDetail(){
	$.ajax({
		url: '/getAccess',
		data: $('form').serialize(),
		type: 'POST',
		success: function(response){
			console.log(response);
			var responseobj = JSON.parse(response);
			if(responseobj.error){
				$("#error-alert").fadeTo(2000, 500).slideUp(500, function(){
					$("#error-alert").slideUp(500);
				});
			}
			else{
				$("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
					$("#success-alert").slideUp(500);
				});
			}
		},
		error: function(error){
			console.log(error);
			$("#error-alert").fadeTo(2000, 500).slideUp(500, function(){
				$("#error-alert").slideUp(500);
			});
		}
	});
}

$(function(){
	$("#success-alert").hide();
	$("#error-alert").hide();

	$(document).on({
		ajaxStart: function() { $("body").addClass("loading"); },
		ajaxStop: function() { $("body").removeClass("loading"); }    
	});

	$("#inputPassword").keydown(function (e) { 
		if(e.which == 13) {
			e.preventDefault(); 
			submitFormDetail();
		}
	});

	$('#submit').click(function(){
		submitFormDetail();
	});
});
