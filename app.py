from __future__ import print_function
import sys
import time
import os
import atexit
from flask import Flask, render_template, json, request
from werkzeug import generate_password_hash, check_password_hash
from dotenv import load_dotenv
try:
    import CHIP_IO.GPIO as GPIO
except ImportError, e:
    import MockGPIO as GPIO
    #pass # module doesn't exist, deal with it.

#from __future__ import print_function
#import sys

#print('This is error output', file=sys.stderr)
#print('This is standard output', file=sys.stdout)

#load_dotenv(dotenv_path='.env',verbose=True)
load_dotenv(dotenv_path='.env')
app = Flask(__name__)
#app.config.from_pyfile('.env', silent=True)

def getConfiguration(envVariable):
    VARIABLE = os.getenv(envVariable)
    #VARIABLE = app.config[envVariable]
    if VARIABLE is None:
        raise ValueError("No "+ envVariable + " set")
    return VARIABLE;

#Make sure variable exist
GPIO_PIN = getConfiguration("GPIO_PIN")
ACCESS_PASSWORD =getConfiguration("ACCESS_PASSWORD")

@app.route('/')
def main():
    return render_template('index.html')


@app.route('/getAccess', methods=['POST'])
def getAccess():
    try:
        _password = request.form['inputPassword']
        #app.logger.info('%s _password recieved', _password)

        # validate the received values
        if _password:

            #_hashed_password = generate_password_hash(_password)
            #return json.dumps({'message':str(_hashed_password)})
            
            if check_password_hash(ACCESS_PASSWORD, _password):
                sendGpioSignal()
                return json.dumps({'message':'MessageSend'})
            else:
                return json.dumps({'error':'Password invalid'})
        else:
            return json.dumps({'error':'<span>Enter the required fields</span>'})

    except Exception as e:
        return json.dumps({'error':str(e)})
    #finally:


def sendGpioSignal():
    print('sendGpioSignal', file=sys.stdout)
    GPIO.output(GPIO_PIN, GPIO.HIGH)
    time.sleep(1)
    GPIO.output(GPIO_PIN, GPIO.LOW)

def setupGpio():
    print('setupGpio', file=sys.stdout)
    #https://github.com/xtacocorex/CHIP_IO
    # Enable Debug
    #GPIO.toggle_debug()
    GPIO.setup(GPIO_PIN, GPIO.OUT)
    GPIO.output(GPIO_PIN, GPIO.LOW)
    # Clean up every exported GPIO Pin
    #GPIO.cleanup()

def cleanupGpio():
    print('cleanupGpio', file=sys.stdout)
    # Clean up a single pin (keeping everything else intact)
    GPIO.cleanup(GPIO_PIN)

atexit.register(cleanupGpio)
setupGpio()

if __name__ == "__main__":
    app.run(host='0.0.0.0',port=80,use_reloader=False)
