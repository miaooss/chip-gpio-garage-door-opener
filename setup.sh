#!/bin/bash
sudo apt-get update
sudo apt-get install git build-essential python-dev python-pip flex bison chip-dt-overlays -y
#https://github.com/xtacocorex/CHIP_IO
git clone git://github.com/xtacocorex/CHIP_IO.git
cd CHIP_IO
sudo python setup.py install
cd ..
rm CHIP_IO
pip install Flask
pip install -U python-dotenv
pip install waitress

#https://arduino103.blogspot.com/2017/10/systemd-demarrer-une-application-python.html
sudo cp garagedoor.service /etc/systemd/system/garagedoor.service
sudo chown root:root /etc/systemd/system/garagedoor.service
sudo chmod 666 /etc/systemd/system/garagedoor.service
sudo systemctl enable garagedoor.service

#sudo systemctl disable garagedoor.service # (disable pour désactiver le service au prochain boot).
#La commande suivante permet de consulter le statut du service:
#sudo service garagedoor status
#Le service peut être interrompu avec:
#sudo service garagedoor stop